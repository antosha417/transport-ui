import './App.css';
import {useState} from "react";
import {
    Button,
    Container,
    Dropdown,
    Form,
    FormControl,
    Row,
    Col,
    FormGroup,
} from 'react-bootstrap';
import '../node_modules/react-vis/dist/style.css';

import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    LineMarkSeries, DiscreteColorLegend,
} from 'react-vis';

const zip = (a, b) => Array.from(Array(Math.max(b.length, a.length)), (_, i) => [a[i], b[i]]);

function App() {

    const [parameters, setParameters] = useState([]);

    const [graphData, setGraphData] = useState(false);
    // const [graphData, setGraphData] = useState(
    //     {
    //         "infected": [2, 5, 6, 8, 10, 15, 15, 16, 19, 20],
    //         "transmitters": [20, 20, 20, 20, 20, 20, 20, 20, 20, 20],
    //         "deceased": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    //         "cured": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    //         "quarantined": [0, 1, 1, 4, 5, 6, 8, 11, 12, 12],
    //         "contacted": [22, 25, 26, 28, 30, 35, 35, 36, 39, 40]
    //     }
    // );

    const [epochs, setEpochs] = useState(1);
    const [x, setX] = useState(0);
    const [z, setZ] = useState(10000);
    const [S, setS] = useState(100);
    const [j, setJ] = useState(0.2);
    const [u, setU] = useState(0.2);
    const [initInfected, setInitInfected] = useState(4);

    const fileName = parameters.length
        ? `${parameters.slice(0, 5).join('-')}(${parameters[parameters.length - 1]})`
        : ''
    const hardcodedParameters = [
        [0, 0, 100, 0.2, 0.2, 50],
        [10, 0, 10, 0.2, 0.2, 500],
        [10, 0, 100, 0.2, 0.2, 50],
        [10, 0, 100, 0.2, 0.2, 500],
        [10, 0, 100, 0.2, 0.8, 500],
        [10, 0, 100, 0.8, 0.2, 500],
        [10, 1000, 100, 0.2, 0.2, 50],
        [10, 10000, 100, 0.2, 0.2, 50],
        [20, 1000, 100, 0.2, 0.2, 50],
    ];
    const parameterNames = ['x', 'z', 'S', 'j', 'u', 'init_infected'];
    return <>
        <Container>
            <Row>
                <h1>Transport-networks</h1>
                <p>Modeling COVID-19 disease spread via Brownian Motion in multiple cities connected as a hub and spoke
                    structure.</p>
            </Row>
            <Row>
                <a href={"https://www.youtube.com/watch?v=gxAaO2rsdIs"}>Inspired by following SIR
                    model</a>
            </Row>
            <Row>
                <a href="/description.pdf" target="blank">Description in pdf</a>
            </Row>
            <Row>
                <p><a href="https://github.com/valentine-akhiarov/Transport-networks">Github link</a></p>
                <div>
                    <img src="https://miro.medium.com/max/644/1*Qp_H2Ql11172xw5NBqseWA.png"
                         alt="Hub and spoke structure"/>
                </div>
            </Row>
            <Row>
                <Col>
                    <Dropdown>
                        <Dropdown.Toggle variant="success" id="dropdown-basic">
                            {parameters.length
                                ? 'Parameters: ' + zip(parameterNames, parameters).map(x => x[0] + '=' + x[1]).join(', ')
                                : 'Select already calculated parameters ' + parameterNames.join(', ')}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            {hardcodedParameters.map(p =>
                                <Dropdown.Item key={p.join('-')}
                                               onSelect={() => setParameters(p)}>
                                    {p.join(', ')}
                                </Dropdown.Item>
                            )}
                        </Dropdown.Menu>
                    </Dropdown>
                </Col>
                <Col>
                    {!!parameters.length &&
                    <Button href={`/data/${fileName}.csv`} download>Download CSV</Button>}
                </Col>
            </Row>
        </Container>

        {!!parameters.length && <img
            className={'Graph'}
            src={`/data/${fileName}.png`}
            alt={'graph'}
        />}
        <br/>
        <Container>
            <Row>
                <Col>
                    <h4>Or enter custom parameters</h4>
                    <FormGroup className="mb-3">
                        <FormControl type="number" placeholder="epochs"
                                     onChange={e => setEpochs(e.target.value)}/>
                        <Form.Text className="text-muted">
                            For higher number of epochs simulation will take longer. 10 epochs ~ 5 minutes.
                            More than 10 epochs is not recommended, sorry. If you want to run a lot of epochs
                            send email
                            to <a href="mailto: vlad_gezha@mail.ru"> vlad_gezha@mail.ru </a>
                            and we will add simulation to already calculated.
                        </Form.Text>
                    </FormGroup>

                    <FormGroup className="mb-3">
                        <FormControl
                            type="number"
                            placeholder='x'
                            onChange={e => setX(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Number of tests for visible transmitters to have possibility to move to quarantine zone with
                            less death rate
                        </Form.Text>
                    </FormGroup>
                    <FormGroup className="mb-3">
                        <FormControl
                            type="number"
                            placeholder='z'
                            onChange={e => setZ(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Number of tests for others to have possibility to move to quarantine zone with less death
                            rate
                        </Form.Text>
                    </FormGroup>
                    <FormGroup className="mb-3">
                        <FormControl
                            type="number"
                            placeholder='S'
                            onChange={e => setS(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Quarantine zone's capacity
                        </Form.Text>
                    </FormGroup>
                    <FormGroup className="mb-3">
                        <FormControl
                            type="number"
                            placeholder='j'
                            step="0.01"
                            onChange={e => setJ(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Fraction of remote workers
                        </Form.Text>
                    </FormGroup>
                    <FormGroup className="mb-3">
                        <FormControl
                            type="number"
                            placeholder='u'
                            step="0.01"
                            onChange={e => setU(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Fraction of responsible people (which have lower probability of getting ill)
                        </Form.Text>
                    </FormGroup>
                    <FormGroup className="mb-3">
                        <FormControl
                            type="number"
                            placeholder='init_infected'
                            onChange={e => setInitInfected(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Initial infected people number. 1 unit = 100 people
                        </Form.Text>
                    </FormGroup>

                    <FormGroup className="mb-3">
                        <Button variant="primary"
                                type="submit"
                                onClick={() =>
                                    fetch(`https://functions.yandexcloud.net/d4e1goki4jgpnoqj9t5v?epochs=${epochs}&x=${x}&z=${z}&S=${S}&j=${j}&u=${u}&initInfected=${initInfected}`)
                                        .then(response => response.json())
                                        .then(data => {
                                            console.log(data);
                                            return data
                                        })
                                        .then(data => setGraphData(data))
                                }> Submit </Button>
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                {graphData && <DiscreteColorLegend items={[
                    {
                        title: 'Infected People',
                        color: 'blue'
                    },
                    {
                        title: 'Deceased',
                        color: 'black'
                    },
                    {
                        title: 'Cured',
                        color: 'green'
                    },
                    {
                        title: 'Quarantined',
                        color: 'orange'
                    },
                    {
                        title: 'All transmitters (visible + invisible)',
                        color: 'red'
                    },
                    {
                        title: 'People that contacted a disease',
                        color: 'violet'
                    }
                ]}/>}
                {graphData && <>
                    <h6>People by groups vs. time</h6>
                    <XYPlot width={1200} height={600}>
                        <VerticalGridLines/>
                        <HorizontalGridLines/>
                        <XAxis/>
                        <YAxis/>
                        <LineMarkSeries
                            style={{
                                strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'blue'}}
                            markStyle={{stroke: 'blue'}}
                            data={graphData.infected.map((y, i) => ({x: i, y: y}))}
                        />
                        <LineMarkSeries
                            style={{
                                strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'red'}}
                            markStyle={{stroke: 'red'}}
                            data={graphData.transmitters.map((y, i) => ({x: i, y: y}))}
                        />
                        <LineMarkSeries
                            style={{
                                strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'black'}}
                            markStyle={{stroke: 'black'}}
                            data={graphData.deceased.map((y, i) => ({x: i, y: y}))}
                        />
                        <LineMarkSeries
                            style={{
                                strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'green'}}
                            markStyle={{stroke: 'green'}}
                            data={graphData.cured.map((y, i) => ({x: i, y: y}))}
                        />
                        <LineMarkSeries
                            style={{
                                strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'orange'}}
                            markStyle={{stroke: 'orange'}}
                            data={graphData.quarantined.map((y, i) => ({x: i, y: y}))}
                        />
                    </XYPlot>
                </>
                }

                {graphData && <>
                    <h6>People than contacted a disease vs. time</h6>
                    <XYPlot width={1200} height={600}>
                        <VerticalGridLines/>
                        <HorizontalGridLines/>
                        <XAxis/>
                        <YAxis/>
                        <LineMarkSeries
                            style={{
                                strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'violet'}}
                            markStyle={{stroke: 'violet'}}
                            data={graphData.contacted.map((y, i) => ({x: i, y: y}))}
                        />
                    </XYPlot>
                </>
                }
            </Row>
        </Container>
    </>;
}

export default App;
